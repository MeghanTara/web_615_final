# Preliminary Notes

## Spacex
- [SpaceX](http://www.spacex.com/)
- [Falcon Heavy, Rocket](http://www.spacex.com/falcon-heavy)
- [Dragon, Spacecraft](http://www.spacex.com/dragon)
- Youtube Videos
    - [Musk giving a tour of SpaceX, video](https://www.youtube.com/watch?v=TQ6tZtGrShg)
    - [Falcon 1 flights 1-5, videos](https://www.youtube.com/channel/UCEmPjPd1gmUbJpbLP0Kmd0A)
    - [Elon Musk: The Case for Mars](https://youtu.be/Ndpxuf-uJHE)
    
- Investors.com, [SpaceX May Be A Bargain for NASA, If Musk Delivers](http://www.investors.com/why-spacex-may-be-a-bargain-for-nasa/)
- Space.com, [Wow! SpaceX Lands Orbital Rocket Successfully in Historic First](http://www.space.com/31420-spacex-rocket-landing-success.html)

## Daily Mail
- [Rockets of the World, image](http://i.dailymail.co.uk/i/graphics/2015/02/space_shuttles_triple/images/small/small.png)



## Wikipedia
- [SpaceX](https://en.wikipedia.org/wiki/SpaceX)
- [Elon Musk](https://en.wikipedia.org/wiki/Elon_Musk)
- [Falcon 1](https://en.wikipedia.org/wiki/Falcon_1)
- [Falcon 9](https://en.wikipedia.org/wiki/Falcon_9)
- [Merlin engine](https://en.wikipedia.org/wiki/Merlin_(rocket_engine_family))

## TechCrunch
- [SpaceX Stories](http://techcrunch.com/topic/company/spacex/)

## Wait But Why
### The World's Raddest Man Series
- [Part 1](http://waitbutwhy.com/2015/05/elon-musk-the-worlds-raddest-man.html), Musk
- [Part 2](http://waitbutwhy.com/2015/06/how-tesla-will-change-your-life.html), Tesla
- [Part 3](http://waitbutwhy.com/2015/08/how-and-why-spacex-will-colonize-mars.html), SpaceX
- [Part 4](http://waitbutwhy.com/2015/11/the-cook-and-the-chef-musks-secret-sauce.html), Musk Again

## Cosmos Magazine
- [The big five extinctions](https://cosmosmagazine.com/earth-sciences/big-five-extinctions)

## Universe Today
- [The Life Cycle of the Sun](http://www.universetoday.com/18847/life-of-the-sun/)

## MIT Technology Review
- [Time Likely To End Within Earth's Lifespan, Say Physicists](https://www.technologyreview.com/s/420963/time-likely-to-end-within-earths-lifespan-say-physicists/)

## Wired
- [Elon Musk's Mission to Mars](http://www.wired.com/2012/10/ff-elon-musk-qa/all/)
- [Space is Cold, Vast, and Deadly. Humans Will Explore It Anyway](http://www.wired.com/2016/02/space-is-cold-vast-and-deadly-humans-will-explore-it-anyway)

## Spaceflight Now
- [Sweet success at last for Falcon 1 rocket](http://spaceflightnow.com/falcon/004/)

## Vigrin Galactic
- [Virgin Galactic's New Spaceship Puts It Back in the Space Race](http://www.wired.com/2016/02/live-unveiling-spaceship-two-virgin-back-space-game/)

## Blue Origin
- [Blue Origin's Reusable Rocket Just Struck its Third Landing](http://www.wired.com/2016/04/blue-origins-reusable-rocket-just-stuck-third-landing/)