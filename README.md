# Web 615 Final Paper

## SpaceX

Humanity will, one day, perish, be it hundreds, thousands, or billions of years from now. 

SpaceX, driven by Elon Musk -- along with a veritable army of incredibly capable and talented people -- seeks to push this expiry date to its highest limit, through enabling humans to become multi-planetary, decoupling the survival of humanity and the survival of Earth. This does, at first glance, appear impossible, but what began as a far-fetched plan to put a vegetable garden on Mars has turned into a thriving aerospace manufacturing and space transport services company, with an ultimate goal of colonizing Mars ([Wired: Elon Musk's Mission to Mars](http://www.wired.com/2012/10/ff-elon-musk-qa/all/)).

### When They Began

In 2002, after selling PayPal, Musk decided to pursue his interest in space. Upon realizing that NASA had greatly reduced the scope of its efforts, and had no plan to go to Mars, he came up with an idea for the *Mars Oasis* missions: to put a greenhouse on Mars, "... to spur the national will", and gather "a lot of engineering data about what it takes to maintain a little greenhouse and keep plants alive on Mars".

In pricing out the missions, Musk found that the most expensive component would be the rockets -- "the actual propulsion from Earth to Mars", and that they were so costly as to be prohibitive. And so he took the pulse of the space industry, gathered a team of "technically astute" professionals, and "had them do a feasibility study about building rockets more efficiently". Once it became clear that it was possible to build cheaper rockets, SpaceX was founded ([Wired: Elon Musk's Mission to Mars](http://www.wired.com/2012/10/ff-elon-musk-qa/all/)).

### Who They Are

According to their website, "SpaceX designs, manufactures and launches advanced rockets and spacecraft. The company was founded in 2002 to revolutionize space technology, with the ultimate goal of enabling people to live on other planets". SpaceX was founded by Elon Musk, the company's CEO and Lead Designer. The company employs more than four thousand people, and is headquartered in Hawthorne, California, with "launch facilities at Cape Canaveral Air Force Station, Florida, and Vandenberg Air Force Base, California", offices in Houston, Texas, and "a rocket-development facility in McGregor, Texas" ([SpaceX](http://www.spacex.com/about)).

### What They Do

The ultimate goal of SpaceX is to enable humanity to become multi-planetary. Musk argues that this goal is, or should be, a step on the scale of the evolution of life ([Elon Musk: The Case for Mars](https://youtu.be/Ndpxuf-uJHE)). According to Musk, Mars is the first planet, other than Earth, that humans should seek to occupy. Successful occupation is defined as having one million individuals living on the planet -- this is the number that Musk believes will allow the population to be self-sustainable -- able to provide for all of its needs without assistance from Earth ([How (and Why) SpaceX Will Colonize Mars, Part 2: Musk's Mission](http://waitbutwhy.com/2015/08/how-and-why-spacex-will-colonize-mars.html/2)).

### Why They Do What They Do

Given that mass extinction events have occurred regularly since the inception of life on our planet ([The big five extinctions](https://cosmosmagazine.com/earth-sciences/big-five-extinctions)) -- whether through the occurrence of a close supernova, a gamma-ray burst, a super solar flare, the reversal of the Earth's magnetic field, a rogue black hole, a global epidemic, or collision with an asteroid --  it follows that they will continue to happen regularly while our planet exists. If this is true, humanity, as it exists on Earth, has a lifespan. In addition, our Sun, itself, has a lifespan ([The Life Cycle of the Sun](http://www.universetoday.com/18847/life-of-the-sun/)). 

This means that even if humanity were to survive all possible future mass extinction events on Earth, our lifespan is still tied to that of the Sun. It would behoove us -- us being humanity in its entirety -- to find a way to exist successfully on other planets, and, eventually, around other stars, should we want to improve our chances of existing relatively far into the future ([How (and Why) SpaceX Will Colonize Mars, Part 2: Musk's Mission](http://waitbutwhy.com/2015/08/how-and-why-spacex-will-colonize-mars.html/2)). It should be noted that the time scale on which this argument is laid out is massive, but that, in and of itself, does not nullify its claims.

The universe itself also has a lifespan, and so it should be noted that this paper is concerned with, and limited to, the possibilities as they exist while the universe continues to exist ([Time Likely To End Within Earth's Lifespan, Say Physicists](https://www.technologyreview.com/s/420963/time-likely-to-end-within-earths-lifespan-say-physicists/), MIT Technology Review).

Exploration itself is also a valid reason for putting effort towards becoming multi-planetary -- even without achieving it, we stand to benefit collectively from the gains made in its pursuit.

### How They Do What They Do

In order to achieve the goal of getting one million people on Mars, SpaceX first needed to develop the ability to launch rockets into space. After doing so, it needed to find clients who would hire SpaceX to deliver cargo (whether inanimate or human) into space (or low orbit), so that the company could continue to bring the cost of launch down to the point where a ticket to Mars would be affordable by enough people to reach the one million passenger mark.

In going about making cheaper, more efficient rockets, SpaceX has revolutionized the aerospace industry. The first method employed in cost reduction is keeping the company vertically integrated, eliminating the overhead brought into the cost by outsourcing parts and services ([SpaceX May Be A Bargain for NASA, If Musk Delivers](http://www.investors.com/why-spacex-may-be-a-bargain-for-nasa/)).

The second method employed in cost reduction is making the production method itself more efficient. In an interview with Wired, in 2012, Musk contrasted SpaceX's process for manufacturing the airframe of the rocket with the process that came before it:

> Traditionally, a rocket airframe is made by taking an aluminum plate perhaps a couple of inches thick and machining deep pockets into it. Then you’ll roll or form what’s left into the shape you want — usually sections of a cylinder, since rockets tend to be primarily cylindrical in shape. That’s how Boeing and Lockheed’s rockets are made, and most other rockets too. But it’s a pretty expensive way to do it, because you’re left with a tiny fraction of the plate’s original mass.

In addition to being wasteful, this process is time consuming.

SpaceX's approach to the construction of the airframe is to cut grooves into the aluminum, fold it together, then weld it into place using "advanced welding technology called stir welding":

> Instead of riveting the ribs and hoops, you use a special machine that softens the metal on both sides of the joint without penetrating it or melting it. Unlike traditional welding, which melts and potentially compromises some metals, this process works well with high-strength aluminum alloys. You wind up with a stiffer, lighter structure than was possible before. And your material loss is maybe 10 percent, just for trimming the edges. Instead of a ratio of purchased to flown material — what they call the “buy to fly” ratio — of maybe 10 to 20, you have a ratio of 1.1, 1.2 tops ([Wired: Elon Musk's Mission to Mars](http://www.wired.com/2012/10/ff-elon-musk-qa/all/)).

The third way that SpaceX is revolutionizing production is through making its rockets reusable. Upon launching the rocket, once the first stage has left the Earth's atmosphere, it separates from the second stage. The second stage continues on its way, to bring the payload to its destination in orbit, while the first stage flips around, and then uses its engines with great precision, to decelerate and land the rocket vertically ([How (and Why) SpaceX Will Colonize Mars, Part 4: Musk's Mission](http://waitbutwhy.com/2015/08/how-and-why-spacex-will-colonize-mars.html/4)).

SpaceX landed the first stage of their Falcon 9 rocket, successfully, for the first time, on December 22, 2015, in a mission delivering eleven commercial satellites into orbit for their client, ORBCOMM ([Wow! SpaceX Lands Orbital Rocket Successfully in Historic First](http://www.space.com/31420-spacex-rocket-landing-success.html)). SpaceX achieved another successful landing of a Falcon 9 again, on April 8, 2016, after launching its Dragon cargo spacecraft to the International Space Station ([CRS-8 | First Stage Landing on Droneship](https://www.youtube.com/watch?v=sYmQQn_ZSys)). Among the cargo was the Bigelow Expandable Activity Module (BEAM), which will be attached to the ISS, in order to test its capabilities ([NASA To Attach First Inflatable Habitat To The ISS This Weekend](http://www.iflscience.com/space/inflatable-habitat-be-attached-iss-first-time-weekend)).

In an interview, after the landing of the Falcon 9 on April 8th, Musk explained that SpaceX plans to prepare the booster for reuse, and should be able to use it again in a few months -- in the future, the turnaround time should be a matter of weeks ([Elon Musk Discusses CRS-8 Successes with Media](https://www.youtube.com/watch?v=VNygOavo2mY)).

## Competitors / Colleagues

### Virgin Galactic

Virgin Galactic is also operating in the aerospace industry, but with a view towards enabling space tourism, bringing astronauts into space to experience weightlessness and the view of Earth from space, and then returning to Earth ([Your Flight To Space: This is How You Will Become An Astronaut](http://www.virgingalactic.com/human-spaceflight/your-flight-to-space/)).

### Blue Origin

Blue Origin has also successfully landed a rocket after launching it, but only after launch into suborbit, which is a less difficult feat than that achieved by SpaceX in the landings of their Falcon 9 ([Wow! SpaceX Lands Orbital Rocket Successfully in Historic First](http://www.space.com/31420-spacex-rocket-landing-success.html)).

## Most Pressing/Significant Challenges at Present

In addition to continuing to land rockets successfully after launching them, SpaceX must be able to reuse the rockets. According to Musk, "rapid and complete reusability is the thing that's really important for the reusability to be cost effective -- like an aircraft" ([Elon Musk Discusses CRS-8 Successes with Media](https://www.youtube.com/watch?v=VNygOavo2mY)). Without achieving reusability, even if SpaceX were to be able to bring passengers to Mars, the price of each ticket would be astronomically high.

The next challenge presented to SpaceX is that of manned space travel. The company is currently working on the second version of their spacecraft, named Dragon 2, which will have accommodations for passengers, and functionality to maneuver in space, land with propulsion, and propel itself away from the launch site, and out of danger, should a catastrophic error occur. The Dragon 2 will also be able to dock with the ISS of its own accord (at present, it must be pulled in to dock to the ISS by the ISS) ([How (and Why) SpaceX Will Colonize Mars, Part 4: Musk's Mission](http://waitbutwhy.com/2015/08/how-and-why-spacex-will-colonize-mars.html/4)).

Another challenge is that of capacity -- at present, the Falcon 9 is capable of bringing a payload of 13 150 kilograms to Low Earth Orbit (LEO), and 4 850 kilograms to Geosynchronous Transfer Orbit (GTO) ([Falcon 9](http://www.spacex.com/falcon9)). The Falcon Heavy, currently in production, with plans to launch later this year, will be capable of a 53 000 kilogram payload to LEO, a 21 200 kilogram payload to GTO, and a 13 200 kilogram payload to Mars ([Falcon Heavy](http://www.spacex.com/falcon-heavy)).

If these challenges are surmounted, the next step for SpaceX will be sending unmanned missions to Mars. First will be a test mission, to make sure it is possible to send a spacecraft to Mars and bring it back again. After that will come a series of cargo missions, still unmanned, to bring the equipment and supplies that will be required by the forthcoming manned missions. While Earth and Mars have wildly different orbits around the Sun, approximately every twenty-six months they reach a point of being relatively close to one another (between fifty-five and one hundred million kilometers apart). It is in this window that missions will be sent from Earth to Mars. It is likely that the first unmanned cargo mission will take place in 2018, and the first manned mission will take place 2025 or 2027. In short, the challenges yet to be overcome are massive, but seem potentially surmountable, in light of what has already been achieved ([How (and Why) SpaceX Will Colonize Mars, Part 5: Musk's Mission](http://waitbutwhy.com/2015/08/how-and-why-spacex-will-colonize-mars.html/5)).

